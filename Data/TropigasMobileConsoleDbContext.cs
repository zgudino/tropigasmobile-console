﻿using Microsoft.EntityFrameworkCore;
using System;
using TropigasMobile.Console.Data.Entities;

namespace TropigasMobileConsole.Data
{
    public class TropigasMobileConsoleDbContext : DbContext
    {
        public TropigasMobileConsoleDbContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            base.OnConfiguring(builder);
        }

        public DbSet<SyncEventInput> SyncEventInputs { get; set; }
        //TODO: Missing AspNetUsers DbSet (highly arguable)
    }
}
