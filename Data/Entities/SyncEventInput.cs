﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TropigasMobile.Console.Data.Entities
{
    public class SyncEventInput
    {
        public long Id { get; set; }
        public string EntityId { get; set; }
        public string EntityName { get; set; }
        public string EventName { get; set; }
        public string Comments { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ProcessedAt { get; set; }
        public string ProcessedBy { get; set; }
    }
}
