﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TropigasMobileConsole.Data;

namespace TropigasMobileConsole.Cli.NetCore
{
    enum ActionEnums
    {
        UNCONFIRMED_ACCOUNTS,
        SYNC_EVENT_INTPUTS
    }

    class Program
    {
        private static IConfigurationRoot _configuration;
        public static string _rootPath { get; private set; }

        private const string UNCONFIRMED_ACCOUNT = "/ua";

        static void Main(string[] args)
        {
            _rootPath = Directory.GetCurrentDirectory();

            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .WriteTo.File($"{_rootPath}/Logs/SynEventInputs.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();

            var builder = new ConfigurationBuilder()
                .SetBasePath(_rootPath)
                .AddEnvironmentVariables()
                .AddJsonFile("AppSettings.json", true, true);
#if DEBUG
            builder.AddUserSecrets<Program>();
#endif
            _configuration = builder.Build();

            var opts = args.Any() ? args[0] : "";

            switch (opts)
            {
                case UNCONFIRMED_ACCOUNT:
                    //TODO: Missing `DeleteUnconfirmedAccountAsync` Runtime (ie. Service)
                    break;

                default:
                    //TODO: Move as Runtime (ie. Service)
                    SyncEventInputsAsync().Wait();
                    break;
            }

            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }

        static Task SyncEventInputsAsync()
        {
            // TODO: Tightly coupled! IoC is better approach
            var apiUrl = _configuration["TropigasMobileBackend:ApiUrl"];
            var apiKey = _configuration["TropigasMobileBackend:ApiKey"];
            var connectionString = _configuration.GetConnectionString("TropigasMobileDb");
            var httpClient = new HttpClient();

            Log.Information("== INICIO ==");
            Log.Information("ApiUrl {ApiUrl}", apiUrl);
            Log.Information("ApiKey {ApiKey}", apiKey);

            httpClient.DefaultRequestHeaders.Clear();
            httpClient.DefaultRequestHeaders.Add("X-ApiKey", apiKey);

            var cancellationTokenSource = new CancellationTokenSource();
            CancellationToken cancellationToken = cancellationTokenSource.Token;

            return Task.Run(async () =>
            {
                try
                {
                    Log.Information("Estableciendo conexión RDBMS");

                    var optionBuilder = new DbContextOptionsBuilder().UseSqlServer(connectionString);

                    using (var db = new TropigasMobileConsoleDbContext(optionBuilder.Options))
                    {
                        cancellationToken.ThrowIfCancellationRequested();

                        var records = await db.SyncEventInputs.AsNoTracking()
                                              .Where(r => r.ProcessedAt == null)
                                              .Take(5)
                                              .ToListAsync();

                        Log.Information("{Count} fueron extraidos", records.Count());

                        if (records.Any())
                        {
                            foreach (var record in records)
                            {
                                var resource = $"{apiUrl}/{record.EntityName}/{record.EventName}/{record.Id}";

                                Log.Information("GET {Resource}", resource);
                                Log.Information("{@Record}", record);

                                var resp = await httpClient.GetAsync(resource);

                                Log.Information("Response {StatusCode}", resp.StatusCode);

                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Fatal(ex.Message);

                    cancellationTokenSource.Cancel();
                }
                finally
                {
                    cancellationTokenSource.Dispose();
                }

                Log.Information("== FIN ==");

            }, cancellationToken);
        }
    }
}
